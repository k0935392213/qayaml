#!/bin/bash

mkdir -p /etc/redis
mkdir -p /data/opt/redis
mkdir -p /data/opt/logs/redis

# git clone https://gitlab.com/k0935392213/qayaml.git
cd
cd $PWD/qayaml/install
tar xvf redis-5.0.7.tar.gz
cd redis-5.0.7
make && make install
cd ..
\cp -p $PWD/redis.conf /etc/redis/redis.conf

cat << EOF >/etc/systemd/system/redis.service
[Unit]
Description=redis-server
After=network.target

[Service]
Type=forking
ExecStart=/usr/local/bin/redis-server /etc/redis/redis.conf
PrivateTmp=true

[Install]
WantedBy=multi-user.target
EOF

sed -i 's/^daemonize no/daemonize yes/' /etc/redis/redis.conf

cat >> /etc/sysctl.conf << EOF
vm.overcommit_memory=1
net.core.somaxconn = 2048
EOF

echo never > /sys/kernel/mm/transparent_hugepage/enabled
systemctl daemon-reload
systemctl start redis.service
systemctl enable redis.service

