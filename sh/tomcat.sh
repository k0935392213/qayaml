#!/bin/bash

#chown -R tomcat:tomcat /data/opt/tomcat
#systemctl restart tomcat

#echo "等待tomcat三分鐘，可以先去喝水"
#sleep 180
#cat /data/opt/logs/tomcat/catalina.out |grep '请'
#echo "取得你的東西以後，請通知該通知的地方生成lincense"
#sleep 120

systemctl stop tomcat
echo ' ' > /data/opt/logs/tomcat/catalina.out
rm -fr /data/opt/tomcat/latest/work/*
rm -fr /data/opt/tomcat/latest/temp/*
rm -fr /data/opt/logs/tomcat/catalina.out 

systemctl restart tomcat

sed -i 's|#Port 22|Port 6262|g' /etc/ssh/sshd_config
systemctl restart sshd
systemctl start ipset
systemctl enable ipset

systemctl start iptables
service iptables save
systemctl start iptables
systemctl enable iptables


