#!/bin/bash

setenforce 0
sed -i --follow-symlinks 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/sysconfig/selinux

echo "kernel.sem = 250 32000 100 128" >> /etc/sysctl.conf
echo "net.ipv4.ip_local_port_range = 9000 65500" >> /etc/sysctl.conf
echo "net.core.rmem_default = 4194304" >> /etc/sysctl.conf
echo "net.core.rmem_max = 4194304" >> /etc/sysctl.conf
echo "net.core.wmem_default = 262144" >> /etc/sysctl.conf
echo "net.core.wmem_max = 1048576" >> /etc/sysctl.conf
echo "fs.aio-max-nr = 1048576" >> /etc/sysctl.conf
echo "fs.file-max = 681574" >> /etc/sysctl.conf

echo "* hard nofile 64000" >> /etc/security/limits.conf
echo "* soft nproc 32000" >> /etc/security/limits.conf
echo "* hard nproc 32000" >> /etc/security/limits.conf
echo "* soft nofile 64000" >> /etc/security/limits.conf
echo "mysql hard nofile 64000" >> /etc/security/limits.conf
echo "mysql soft nproc 32000" >> /etc/security/limits.conf
echo "mysql hard nproc 32000" >> /etc/security/limits.conf
echo "mysql soft nofile 64000" >> /etc/security/limits.conf

systemctl stop firewalld
systemctl disable firewalld

yum update -y
yum install nano net-tools -y
yum install git -y
yum install epel-release -y
yum update -y
yum -y groupinstall Development tools
yum -y groupinstall Base
yum -y groupinstall "Compatibility libraries"
yum -y groupinstall "Debugging Tools"

