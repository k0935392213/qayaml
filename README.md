# 這個資料夾內的檔案主要用來安裝 IM 整體架構 
Redis 版本 5.0.7  
Centos7  

# 注意
目前為一台VM，CPU：2、Memory：2

# 請先編輯您的 hostname 作為識別，避免後續資料名稱相同
```sh
vi /etc/hostname
hostname 上面的內容
```

# 執行 run.sh
下載專案
 - 如果沒安裝 git，請先執行 yum install git -y
```sh
git clone https://gitlab.com/k0935392213/qayaml.git
cd qayaml/sh/
sh run.sh
```

會自動幫你設定OS7系統+必要帳號+安裝IM、mysql、redis

#  設定nginx憑證
根據nginx到/etc/nginx/conf.d位置，內有im 以及 websocket資料夾，先對應證書放到對應位置
單機版該修改的就去修改
非單機版有正式證書的到對的地方修改
設定好以後請執行以下指令
```sh
nginx -t 
nginx -s reload
```
#  匯入DB
使用mysql -u root -p 登入以後，將資料匯入，依自己的情境更改指令內容，例如檔案在root，檔名為abc.sql，切換對應DB以後就下source /root/abc.sql
```sh
use im
source $PWD/abc.sql
```

#  執行tomcat

先手動跑前三行，這邊會需要lincense，等到系統跑完以後會通知要去取得，再去取得就好
```sh
chown -R tomcat:tomcat /data/opt/tomcat
systemctl restart tomcat
cat /data/opt/logs/tomcat/catalina.out |grep '请'
```
將取得內容寫入這裡
```sh
vi /data/opt/tomcat/latest/webapps/ROOT/WEB-INF/classes/config/license.properties
```
好了以後運行tomcat.sh，在下指令tail看log檔案內出現"org.apache.catalina.startup.Catalina.start Server startup in xxxxxx ms 才算成功"
```sh
sh tomcat.sh
tail -f /data/opt/logs/tomcat/catalina.out
```
上鎖 tomcat
```sh
chattr +i -R /data/opt/tomcat/latest/webapps/ROOT/
```


